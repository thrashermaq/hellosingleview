//
//  ViewController.swift
//  HelloSingleView
//
//  Created by Nils Müller on 28.10.19.
//  Copyright © 2019 Nils Müller. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func buttonClick(_ sender: Any) {
        print("Hello button")
    }
    
}

